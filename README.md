
**Table of Contents**

- [Gitlab Basics R3 Workshop](#gitlab-basics-r3-workshop)
  - [Prerequisites](#prerequisites)
- [Concepts](#concepts)
  - [Version control](#version-control)
  - [Version control system](#version-control-system)
  - [Remote server](#remote-server)
- [Hands-on](#hands-on)
  - [Preparation](#preparation)
  - [Git basics](#git-basics)
  - [Gitlab basics - connect your project folder](#gitlab-basics---connect-your-project-folder)
    - [Connect to the web interface](#connect-to-the-web-interface)
    - [Add your SSH Key](#add-your-ssh-key)
    - [Create a group](#create-a-group)
    - [Create a new project](#create-a-new-project)
    - [Connect your project](#connect-your-project)
    - [Interact with the server](#interact-with-the-server)
  - [Gitlab basics - collaborate](#gitlab-basics---collaborate)
- [Using git without the terminal](#using-git-without-the-terminal)


# Gitlab Basics R3 Workshop


## Prerequisites

* Basic command line (see [Linux Basics tutorial](https://git-r3lab.uni.lu/tutorial/2015-06-19-R3-Linux101/blob/master/README.md)).


# Concepts

* What is a Version Control System (VCS)?
* What is git?
* What is a remote server?


## Version control

> A component of software configuration management, version control, also known as revision control or source control, is the management of changes to documents, computer programs, large web sites, and other collections of information. Changes are usually identified by a number or letter code, termed the "revision number," "revision level," or simply "revision." we will refers to it as a **commit revision number** or more simply, a **commit**. (Wikipedia)

## Version control system

Tool that will help you doing version control. You can think of it as the **Word Review Tab**.

![Word](img/word-review.png)

We will use one tool that is not word-specific or not software specific but that is widely used in the scientific world.
For instance, the word Github (*a remote server that uses git*) appears more than 700 times in pubmed
only in the Abstract section.

![Github](img/github.png)

We will use **git** as it is one of the most widely used in the world and especially in bioinformatics.


## Remote server

A remote server can be seen as a website that is located somewhere else on a distant computer. For instance,
you can access our Gitlab remote server via this URL: https://git-r3lab.uni.lu/

---
** LIVE Presentation of GITLAB remote server**
---

# Hands-on

## Preparation

* Install git

    * This must be already the case if you are on Mac or Linux
    * If you are using windows use [this link](http://git-scm.com/download/win) to install it. This [video](https://www.youtube.com/watch?v=albr1o7Z1nw) explain the steps to follow.
    (Don't forget to click on the checkbox to add the shortcut on your Desktop)

* Open your terminal:  (see [Linux Basics tutorial](https://git-r3lab.uni.lu/tutorial/2015-06-19-R3-Linux101/blob/master/README.md)).
For Window power users, you can click on the shortcut "Git Bash".

> Note: if you don't have git installed or don't want to use your own laptop, you can use the VM that we have set up for you.
To connect, follow what's [HERE](https://git-r3lab.uni.lu/tutorial/2015-06-19-R3-Linux101/blob/master/README.md#5-connecting-to-virtual-machine)

* Go to your home folder and create a new folder: `GitlabWorkshop`

```bash
$ cd                        # Change directory to your home folder
$ mkdir GitlabWorkshop      # Make a new directory with the name "GitlabWorkshop"
$ cd GitlabWorkshop         # Change directory to the created one
```

> Note: all commands in this tutorial start with the $ sign. If there is not, it is an output that you get from the terminal.

* Add some text file with some content

```bash
$ echo "I am a text file" > README.md
```

* Now you could open the *README.md* file with your favorite text editor (TextPad, TextEdit, ...) or with
[nano](https://git-r3lab.uni.lu/tutorial/2015-06-19-R3-Linux101/blob/master/README.md#text-editor) if you are
stuck on using the terminal.

> Note: REAME.md file is a community standard in the *git* world.
It is the first file that users will see if they go to your projects.
It is always good to have one that describe what your project is about.


## Git basics

Now you want to **enable git** on your project

* Go back to the terminal inside the `GitlabWorkshop` folder


* **But I don't know how what/where/how is my LUMS account !?**


* Initialize git

```bash
$ git init
Initialized empty Git repository in /Users/yohan.jarosz/GitlabWorksop/.git/
```

* Check the status of your project

```bash
$ git status
On branch master

Initial commit

Untracked files:
  (use "git add <file>..." to include in what will be committed)

	README.md

nothing added to commit but untracked files present (use "git add" to track)
```

> Useful command that will tell you what file is tracked or not by git


* Add the file to the tracking system

```bash
$ git add README.md
```

* Check the status again

```bash
$ git status
On branch master

Initial commit

Changes to be committed:
  (use "git rm --cached <file>..." to unstage)

	new file:   README.md
```

What have changed?

Do you understand the difference?

* Save a version of the file and make a new **revision**, a new **commit**.

```bash
$ git commit -am "My first commit"                                                                                                                                                                                                                                                                                                     [master (root-commit) 1286185] My first commit
 1 file changed, 1 insertion(+)
 create mode 100644 README.md
```

> Note: you have to add a comment each time you save a new version of your work

* Check the status again ;)

```bash
$ git status                                                                                                                                                                                                                                                                                                                             On branch master
nothing to commit, working directory clean
```

* Do the same steps again: make some change in the text file with your favorite editor and save a new version.


## Gitlab basics - connect your project folder

Question: Now you have a git project but how you do share it with people from your group?

> Maybe the answer is hidden in the title ;)

### Connect to the web interface

* Connect to Gitlab: https://git-r3lab.uni.lu/ and log in with your LUMS account. If you don't know how what/where/how
is your LUMS account, ask someone from the R3 team to set-up/reset your password.

### Add your SSH Key

This will allow the Gitlab server to know you and accept request from your computer.

* Add your public ssh key to Gitlab

    Profile Settings > SSH Keys > ADD SSH KEY

* If you don't know how what/where/how is your SSH KEY:

    * You can find some info on our [Previous Workshop](https://git-r3lab.uni.lu/tutorial/2015-06-19-R3-Linux101/blob/master/README.md#2-generate-your-ssh-key)
    * You can also find some information [here](https://help.github.com/articles/generating-ssh-keys/)
    * If none of this work, than ask someone to help you ;)

### Create a group

This will allow to manage group of people efficiently for collaboration


* Go to Gitlab (https://git-r3lab.uni.lu) and create a group
    * Gitlab > Groups > NEW GROUP
    * Remember:
        * A group is a collection of several projects
        * Groups are private by default
        * Members of a group may only view projects they have permission to access
        * You could add members to this group to set permissions globally

### Create a new project

* Now create a new project
    * Gitlab > Projects > NEW PROJECT
    * Enter some name there: `GitlabWorkshop`, it will correspond to your folder
    * Change the **namespace** of your project to the group name you just created
    * Change the visibility level if you want


### Connect your project

* Now you can read the instructions on the page of the project, to *add an existing folder or Git repository*.

* Go to your terminal again and add the **Gitlab remote**

```bash
$ git remote add origin ssh://git@git-r3lab-server.uni.lu:8022/<groupname>/GitlabWorkshop.gitranch
```

Now your local project on your computer is connected to the Gitlab server.

> Note: "origin" is the name you give to the remote. It's hard to remember the long url :) As you can have multiple
remotes, it's good to have a name you can remember. By default, it is always "origin".

### Interact with the server

* Push the changes to the server

```bash
$ git push -u origin master
```

* Now you could should be able to see your project on the server: https://git-r3lab.uni.lu/<groupname>/GitlabWorkshop

> Note: "master" is the name of a "git branch". It is another concept of git and wont be covered for now. If you want
to know more, you could read [this](http://www.git-scm.com/book/en/v2/Git-Branching-Basic-Branching-and-Merging). By
default, it is always "master".

## Gitlab basics - collaborate

* Could you give access to your project to your neighbor?

    * Give access directly to one person

        Project > Members > Add Members

    OR

    * Add the person into your created group.

        Group > Members > Add Members

* Now you should have access to a project via Gitlab that is not yours, and you are able to modify it depending on the
permission you have. So start to modify the text file we have created previously.

    * You could modify it via the interface

    OR

    * Clone the project to your local computer !!!

```bash
$ cd                       # go to your home folder
$ mkdir NeighborProject    # create a new folder
$ cd NeighborProject       # go inside that folder
$ git clone ssh://git@git-r3lab-server.uni.lu:8022/<groupnameofyourneighbor>/GitlabWorkshop.git  # clone the project
$ cd GitlabWorkshop        # go inside cloned project
```

At this point you should be able to modify the text file by yourself, or even add new files...

Reminder:

    * git add <file>                      # add a new file
    * git commit -am "A comment"          # commit changes
    * git push                            # push the change to the server

And now, how to get the modifications done by your neighbor into your project?

```bash
$ cd                   # go to your home directory again ;)
$ cd GitlabWorkshop    # go to your project
$ git pull             # Pull the changes from the server !!!
```


You can find Basics git commands and workflows is summarized in those [git cheatsheet](https://www.google.com/search?tbm=isch&q=git%20cheatsheet&tbs=imgo:1).


# Using git without the terminal

There are numerous applications that integrate **git** in their workflow. For instance see [Matlab documentation](http://nl.mathworks.com/help/matlab/matlab_prog/set-up-git-source-control.html),
or [R Studio documentation](https://support.rstudio.com/hc/en-us/articles/200532077-Version-Control-with-Git-and-SVN).
